import numpy as np
import pickle


#this function should return pred_Y_avail and pred_Y_price
#test_X is assumed to be a list of 8 elements [........]

def predictor_blablacar(test_X):
    
    #convert list to array
    test_X = np.array(test_X)
    
    #reshape the array to fit to the network
    test_X = np.reshape(test_X, (1, np.product(test_X.shape[:])))
    
    #load the latest networks
    nn_avail = pickle.load(open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_avail.pkl','rb'))
    nn_price = pickle.load(open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_price.pkl','rb'))
    
    #make the predictions
    pred_Y_avail = nn_avail.predict(test_X) 
    pred_Y_price = nn_price.predict(test_X)
    
    #these predictions are array(1,1), hence need to get them into single number form
    pred_Y_avail = pred_Y_avail[0][0]
    pred_Y_price = pred_Y_price[0][0]
    
    #return the two prediction arrays (basically numbers
    return pred_Y_avail, pred_Y_price

    
#similarly we need to write the predictors for all the websites (megabus, postbus, eurolines...)
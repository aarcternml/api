from pycnic.core import Handler, WSGI
from pycnic.errors import HTTP_400
from loop_predictions import loop_predictor


class QueryHandler(Handler):

    def post(self):
        return loop_predictor(self.request.data)
            
    def get(self):
        return {
            "GET not allowed, please use POST"
        }
        

class app(WSGI):
    routes = [ ("/predict", QueryHandler()) ]
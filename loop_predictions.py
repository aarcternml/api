import json
from predictors import predictor_blablacar #more to follow later
from datetime import *
from pre_process_helper import normalize_features

SECONDS_IN_A_DAY = 86400

def loop_predictor(jsonstring):
    
    #define separate json strings (lists) for all the websites (currently just blablacar)
    json_blablacar = {"searchDay":[], "searchMonth":[], "searchYear":[], "availability":[], "price":[]}
    
    #assuming that we receive a json object as the user query
    contents = jsonstring
    
    #assuming we directly get a dict like {sasdasf:aad, asds:asdasd, ...}, NOT a list of dict like [{asdas:asdasd, sads:asd, ...}]
    #currently our granularity of training is 1 day, hence we take difference in days
    daysdiff = (contents["travelTimestamp"] - contents["searchTimestamp"])/SECONDS_IN_A_DAY
    
    #redefine the current search date; this is going to be modified inside the loop
    currentsearchdate = date(year = contents["searchYear"], month = contents["searchMonth"], day = contents["searchDate"])    
    
    nextdate = timedelta(days = 1)
    
    #start the LOOP from the search date till the travel date
    while (daysdiff>=0):
        daysdiff = daysdiff - 1
        
        
        #get the well-fromulated test query input
        test_X_blabla, junk_Y_avail, junk_Y_price = normalize_features([contents])
                        
            
        #get all the predictions from the test input query (currently only blablacar predcictions)
        pred_Y_avail_blabla, pred_Y_price_blabla = predictor_blablacar(test_X_blabla)
        
        
        #update the websites' json strings (currently only blablacar's)
        json_blablacar["searchDay"].append(currentsearchdate.day)
        json_blablacar["searchMonth"].append(currentsearchdate.month)
        json_blablacar["searchYear"].append(currentsearchdate.year)
        json_blablacar["availability"].append(pred_Y_avail_blabla)
        json_blablacar["price"].append(pred_Y_price_blabla)
                
        
        #also, we need to append the modified search_date into the websites' json strings
        currentsearchdate = currentsearchdate + nextdate      
        
        
        #change the search time stamp for the next iteration in the loop
        contents["searchTimestamp"] = contents["searchTimestamp"] + SECONDS_IN_A_DAY
    
    
    #after the iterations are over, concatenate the results from different websites (currently only blablacar)
    json_final = {"blablacar":json_blablacar}
    
    #ideally, json_final = {'blablacar':json_blablacar, 'megabus':json_megabus, 'postbus':json_postbus .....}
    
    #finally return the full json string
    return json_final
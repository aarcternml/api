from collections import OrderedDict
from datetime import datetime, date

latlongdict = OrderedDict(
[('Amsterdam' ,    [52.370216  ,   4.895168]),

('Zurich' ,       [47.376887  ,   8.541694]),

('Paris' ,        [48.856614  ,   2.352222]),

('Rome' ,         [41.902783  ,  12.496366]),

('Milan' ,        [45.465422  ,   9.185924]),

('Brussel' ,      [50.85034   ,   4.35171]),

('Luxembourg' ,   [49.611621  ,   6.131935]),

('Prague' ,       [50.075538  ,  14.4378]),

('Vienna' ,       [48.208174  ,  16.373819]),

('Warsaw' ,       [52.229676  ,  21.012229]),

('Berlin' ,       [52.520007  ,  13.404954]),

('Hamburg' ,      [53.551085  ,   9.993682]),

('Leipzig' ,      [51.339695  ,  12.373075]),

('Dresden' ,      [51.050409  ,  13.737262]),

('Nuremberg' ,    [49.45203  ,   11.07675]),

('Munich' ,       [48.135125  ,  11.581981]),

('Stuttgart' ,    [48.775846  ,   9.182932]),

('Frankfurt' ,    [50.110922  ,   8.682127]),

('Dusseldorf' ,   [51.227741  ,   6.773456]),

('Hannover' ,     [52.375892  ,   9.73201])]
)


def query2json_converter (query_list):
    
    
    #========BASIC QUERY STUFF===============    
    sourceName = query_list["loc_from"]
    destinationName = query_list["loc_to"]
    travel_date = query_list["date"]
    uniqueSeatsLeft = query_list["seats"]
    #========================================
    
    
    #=========LAT and LONG================================
    sourceLatitude = latlongdict[sourceName][0]
    sourceLongitude = latlongdict[sourceName][1]
    destinationLatitude = latlongdict[destinationName][0]
    destinationLongitude = latlongdict[destinationName][1]
    #=====================================================
    
    
    #========SEARCH and TRAVEL DATES==================================================================    
    searchdt = datetime.utcnow()
    searchDate = searchdt.day
    searchMonth = searchdt.month
    searchYear = searchdt.year
    searchDay = searchdt.isoweekday()
    zerotimestamp = datetime.fromtimestamp(0)
    searchTimestamp = int((searchdt - zerotimestamp).total_seconds())
    
    resultTravelDate = int(travel_date[0:2])
    resultTravelMonth = int(travel_date[3:5])
    resultTravelYear = int(travel_date[6:])
    travelDateObject = date(year = resultTravelYear, month = resultTravelMonth, day = resultTravelDate)
    resultTravelDay = travelDateObject.isoweekday()
    resultTravelTimestamp = int((travelDateObject - date.fromtimestamp(0)).total_seconds())
    #==================================================================================================
    
    
    resultCountFinal = 0
    resultPriceFinal = 0
    
    
    return OrderedDict([("resultAvailability",resultCountFinal), ("resultPrice",resultPriceFinal), 
                ("seatsLeft" , uniqueSeatsLeft), ("sourceName",sourceName),("sourceLatitude",sourceLatitude),
                ("sourceLongitude",sourceLongitude), ("destinationname",destinationName), 
                ("destinationLatitude",destinationLatitude),("destinationLongitude",destinationLongitude),
                ("searchDate",searchDate), ("searchMonth",searchMonth), ("searchYear",searchYear),
                ("searchDay",searchDay), ("searchTimestamp",searchTimestamp), ("travelDay", resultTravelDay),
                ("travelMonth", resultTravelMonth), ("travelDate", resultTravelDate), ("travelYear", resultTravelYear),
                ("travelTimestamp",resultTravelTimestamp)])
    
    
    



# sample.py
import falcon
import json
import query2json
from query2json import query2json_converter
from loop_predictions import loop_predictor
 
class QueryHandler:
    
    def on_get(self, req, resp):

        loc_from = req.get_param('loc_from') or 'Amsterdam'
        loc_to = req.get_param('loc_to') or 'Paris'
        date = req.get_param('date') or '30-11-2015'
        seats = req.get_param_as_int('seats') or 1
        
        input_query = {"loc_from":loc_from, "loc_to":loc_to, "date":date, "seats":seats}
        
        jsoned_query = query2json_converter(input_query)
        
        #jsoned_query = json.dumps(jsoned_query)
        
        predictions = loop_predictor(jsoned_query)

        resp.body = json.dumps(predictions)
        
app = falcon.API()

app.add_route('/predict', QueryHandler())